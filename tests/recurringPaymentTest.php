<?php

require_once dirname(__FILE__).'/mock/functions.php';
require_once dirname(__FILE__).'/mock/database.php';
require_once dirname(__FILE__).'/mock/config.php';

class absences_entryTest extends PHPUnit_Framework_TestCase
{
    
    protected function getAdyen()
    {
        $adyen = bab_functionality::get('Payment/Adyen');
        /*@var $adyen Func_Payment_Adyen */
        $adyen->setConfig(LibPaymentAdyen_getTestConfig());
        
        return $adyen;
    }
    
    

    protected function newTestCard(libpayment_CardPayment $payment)
    {
        $card = $payment->newCard();

        // https://docs.adyen.com/support/integration#testcardnumbers
        
        // Mastercard, FR consumer
        $card->number = '5136333333333335'; 
        
        // Visa, FR Gold
        //$card->number = '4977 9494 9494 9497';
        
        $card->cvc = '737';
        $card->expiryMonth = '08';
        $card->expiryYear = '2018';
        $card->holderName = 'Test LibPaymentAdyen';
        
    
        return $card;
    }
    
    
    protected function newTestPayment()
    {
        $adyen = $this->getAdyen();
        
        $payment = $adyen->newCardPayment();
        $payment->setAmount(1);
        $payment->setCurrency(libpayment_Payment::CURRENCY_EUR);
        $payment->setShopperReference('test@libpaymentadyen');
        $payment->setEmail('libpayment@example.com');
        $payment->setCard($this->newTestCard($payment));
        $payment->setShopperIp('78.231.225.249');
        
        return $payment;
    }
    
    
    
    public function testAuthorizeSimplePayment()
    {
        
    
        $adyen = $this->getAdyen();
        $adyen->doPayment($this->newTestPayment());
    }
    

    /*
    
    public function testAuthorizeRecurringPayment()
    {
        $adyen = $this->getAdyen();
        
        $adyen = $this->getAdyen();
        $adyen->authorizeRecurringPayment($this->newTestPayment());
    }
    */
    
}
