; <?php/*
[general]
name                          = "LibPaymentAdyen"
version                       = "0.1.5"
addon_type                    = "LIBRARY"
mysql_character_set_database  = "latin1,utf8"
encoding                      = "UTF-8"
description                   = "Adyen Payment Gateway functionality"
description.fr                = "Librairie partagée de passerelle de paiement vers Adyen"
delete                        = "1"
longdesc                      = ""
ov_version                    = "8.1.98"
php_version                   = "5.2.0"
addon_access_control          = "0"
configuration_page            = "systemconf"
author                        = "Cantico ( support@cantico.fr )"
icon                          = "logo_adyen.png"
tags						  = "library,payment"

[addons]
LibPayment                    = ">=0.2.13"
widgets						  = ">=1.0.10"
LibTranslate                  = ">=1.12.0rc3.01"

;*/ ?>