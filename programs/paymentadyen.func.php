<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/exceptions.php';
require_once dirname(__FILE__) . '/vendor/autoload.php';
bab_functionality::includeFile('Payment');





class Func_Payment_Adyen extends Func_Payment
{

    /**
     * @var array
     */
    private $recurringOptions = array(
        'contract' => 'RECURRING'
    );

    /**
     * @var LibPaymentAdyen_Configuration
     */
    private $config;



    public function getDescription()
    {
        return LibPaymentAdyen_translate('Adyen Payment Gateway');
    }



    /**
     * @return array
     */
    public function getTpeList()
    {
        $tpeNames = LibPaymentAdyen_getConfigurationNames();

		return $tpeNames;
    }


    /**
     * Set a configuration object
     * usefull for tests only, if not set configuration will be fetched from registry
     * and editable from administrator form
     *
     * @param LibPaymentAdyen_Configuration $config
     */
    public function setConfig(LibPaymentAdyen_Configuration $config)
    {
        $this->config = $config;
    }



    /**
     * Get configuration object
     * @return LibPaymentAdyen_Configuration
     */
    protected function getConfig()
    {
        if (!isset($this->config)) {
            require_once dirname(__FILE__).'/configuration.php';
            $this->config = LibPaymentAdyen_getConfiguration();
        }

        return $this->config;
    }


    /**
     * @return boolean
     */
    public function isTestTpe()
    {
        $config = $this->getConfig();
        return (\Adyen\Environment::TEST === $config->environement);
    }



    /**
     * Get Adyen API client
     * @return \Adyen\Client
     */
    protected function getClient()
    {
        $config = $this->getConfig();

        $client = new \Adyen\Client();
        $client->setApplicationName($config->applicationName);
        $client->setUsername($config->username);
        $client->setPassword($config->password);
        $client->setEnvironment($config->environement);
        $client->setMerchantAccount($config->merchantAccount);

        return $client;

    }



    /**
     * Get Adyen API client for store payout
     * @return \Adyen\Client
     */
    protected function getStorePayoutClient()
    {
        $config = $this->getConfig();

        $client = new \Adyen\Client();
        $client->setApplicationName($config->applicationName);
        $client->setUsername($config->storePayoutUsername);
        $client->setPassword($config->storePayoutPassword);
        $client->setEnvironment($config->environement);
        $client->setMerchantAccount($config->merchantAccount);

        return $client;

    }



    /**
     * Get Adyen API client for review payout
     * @return \Adyen\Client
     */
    protected function getReviewPayoutClient()
    {
        $config = $this->getConfig();

        $client = new \Adyen\Client();
        $client->setApplicationName($config->applicationName);
        $client->setUsername($config->reviewPayoutUsername);
        $client->setPassword($config->reviewPayoutPassword);
        $client->setEnvironment($config->environement);
        $client->setMerchantAccount($config->merchantAccount);

        return $client;

    }





    /**
     * Display a button for hoster payment page
     * @return string
     */
    public function getPaymentRequestHtml(libpayment_Payment $payment)
    {
        throw new Exception('Not implemented, Adyen HPP (hosted payment pages)');

    }

    /**
     * @return array
     */
    private function getAdyenAmount(libpayment_Payment $payment)
    {
        require_once dirname(__FILE__).'/currency.class.php';
        $currency = new LibPaymentAdyen_Currency();

        return array(
            'value' => $currency->encode(
                $payment->getAmount(),
                $payment->getCurrency()
            ),
            'currency' => $payment->getCurrency()
        );
    }


    /**
     * Add payment card to $post
     */
    private function addPaymentCard(Array $post, libpayment_CardPayment $payment)
    {
        $post['threeDS2RequestData'] = array(
            'deviceChannel' => 'browser',
            'notificationURL' => 'https://www.hoper.fr/3ds.php',
            'authenticationOnly' => false
        );

        $screen = $payment->getScreen();

        $post['browserInfo'] = array(
            'userAgent' => $_SERVER['HTTP_USER_AGENT'],
            'acceptHeader' => $_SERVER['HTTP_ACCEPT'],
            'language' => 'fr',
            'colorDepth' => 24,
            'screenHeight' => $screen[1],
            'screenWidth' => $screen[0],
            'timeZoneOffset' => 0,
            'javaEnabled' => false
        );

        bab_debug($post);

        if ($encryptedCard = $payment->getEncyptedCard()) {
            $post['additionalData'] = array(
                'card.encrypted.json' => $encryptedCard
            );

            return $post;
        }

        if ($card = $payment->getCard()) {
            // Not used, require PCI DSS compliance
            $post['card'] = array(
                'number' => $card->number,
                'expiryMonth' => $card->expiryMonth,
                'expiryYear' => $card->expiryYear,
                'cvc' => $card->cvc,
                'holderName' => $card->holderName
            );

            return $post;
        }

        throw new Exception('No card information in card payment');

    }



    private function addPaymentBank(Array $post, libpayment_BankPayment $payment)
    {
        if ($details = $payment->getBankDetails()) {
            $post['bankAccount'] = array(
                'bankAccountNumber' => $details->bankAccountNumber,
                'bankLocationId' => $details->bankLocationId,
                'bankName' => $details->bankName,
                'bic' => $details->bic,
                'countryCode' => $details->countryCode,
                'iban' => $details->iban,
                'ownerName' => $details->ownerName
            );

            return $post;
        }

        throw new Exception('No bank account information in bank payment');
    }


    /**
     * Convert adyen api result to libpayment_Authorization object
     * @param array $result
     * @return libpayment_Authorization
     */
    private function newAdyenAuthorization(Array $result)
    {
        $authorization = $this->newAuthorization();

        $authorization->pspReference = $result['pspReference'];
        if (isset($result['authCode'])) {
            // authCode is not applicable to Payout
            $authorization->authorizationCode = $result['authCode'];
        }
        $authorization->status = $result['resultCode'];

        if (isset($result['refusalReason'])) {
            // managed as an exeption, here for compatiblity with the doc
            // or potential modification of the library
            $authorization->refusalReason = $result['refusalReason'];
        }


        return $authorization;
    }


    /**
     * Log response to payment with reference if possible
     * @param libpayment_Payment    $payment        Payment to log response to
     * @param array                 $response       response from service, mutiple fields
     */
    private function logResponse(libpayment_Payment $payment, $response)
    {
        $reference = null;

        if (isset($response['pspReference'])) {
            $reference = $response['pspReference'];
        }

        $payment->logResponse(print_r($response, true), $reference);
    }

    /**
     * Complete 3D Secure v1 payment after authentication
     */
    public function complete3DSPayment($paymentToken, $md, $paResponse)
    {
        $client = $this->getClient();
        $service = new \Adyen\Service\Payment($client);

        $params = array(
            'md' => $md,
            'paResponse' => $paResponse,
            'shopperIP' => $_SERVER['REMOTE_ADDR']
        );

        // Test a fail

//         $config = $this->getConfig();
//         if ($config->environement === \Adyen\Environment::TEST) {
//             if (!array_key_exists('additionalData', $params)) {
//                 $params['additionalData'] = array();
//             }

//             $params['additionalData']['RequestedTestAcquirerResponseCode'] = 6;
//         }

        $result = $service->authorise3D($params);

        if ($result['resultCode'] === 'Authorised') {
            $authorization = $this->newAdyenAuthorization($result);
            $authorization->isCaptured = false;
            $authorization->token = $paymentToken;

            return $authorization;
        }

        if ($result['resultCode'] === 'Refused') {
            throw new libpayment_Exception(sprintf(LibPaymentAdyen_translate('The payment card has been refused: %s'), $result['refusalReason']));
        }

        throw new libpayment_Exception(sprintf('Unexpected result code %s', $result['resultCode']));
    }

    /**
     *
     * @param string $threeDSCompInd    Y|N
     * @param string $paymentToken
     * @param string $threeDS2Token
     *
     * @return libpayment_Authorization
     */
    public function submitDeviceFingerprint($threeDSCompInd, $paymentToken, $threeDS2Token)
    {
        $client = $this->getClient();
        $service = new \Adyen\Service\Payment($client);

        $result = $service->authorise3DS2(array(
            'threeDS2RequestData' => array(
                'threeDSCompInd' => $threeDSCompInd
            ),
            'threeDS2Token' => $threeDS2Token
        ));

        if ($result['resultCode'] === 'ChallengeShopper') {
            $e = new LibPaymentAdyen_ChallengeRequired();
            $e->additionalData = $result['additionalData'];
            throw $e;
        }

        if ($result['resultCode'] === 'Authorised') {
            $authorization = $this->newAdyenAuthorization($result);
            $authorization->isCaptured = false;
            $authorization->token = $paymentToken;

            return $authorization;
        }

        if ($result['resultCode'] === 'AuthenticationFinished') {  // if authenticationOnly
            $e = new LibPaymentAdyen_AuthenticationFinished();
            $e->additionalData = $result['additionalData'];
            throw $e;
        }

        if ($result['resultCode'] === 'Refused') {
            throw new libpayment_Exception(sprintf(LibPaymentAdyen_translate('The payment card has been refused: %s'), $result['refusalReason']));
        }

        throw new libpayment_Exception(sprintf('Unexpected result code %s', $result['resultCode']));
    }

    /**
     *
     * @param string $transStatus
     * @param string $paymentToken
     * @param string $threeDS2Token
     *
     * @return libpayment_Authorization
     */
    public function submitChallengeResult($transStatus, $paymentToken, $threeDS2Token)
    {
        $client = $this->getClient();
        $service = new \Adyen\Service\Payment($client);

        $result = $service->authorise3DS2(array(
            "threeDS2Result" => array(
                'transStatus' => $transStatus
            ),
            'threeDS2Token' => $threeDS2Token
        ));

        if ($result['resultCode'] === 'Authorised') {
            $authorization = $this->newAdyenAuthorization($result);
            $authorization->isCaptured = false;
            $authorization->token = $paymentToken;

            return $authorization;
        }

        if ($result['resultCode'] === 'AuthenticationFinished') { // if authenticationOnly
            $e = new LibPaymentAdyen_AuthenticationFinished();
            $e->additionalData = $result['additionalData'];
            throw $e;
        }

        if ($result['resultCode'] === 'Refused') {
            throw new libpayment_Exception(sprintf(LibPaymentAdyen_translate('The payment card has been refused: %s'), $result['refusalReason']));
        }

        throw new libpayment_Exception(sprintf('Unexpected result code %s', $result['resultCode']));
    }


    /**
     * Do an authorization on a small amount
     * get back a recurringId for the subsequent payments
     * The payment given in parameter must contain payment mean information
     *
     * The return value will be a libpayment_Authorization
     *
     *
     * @param libpayment_Payment $payment
     * @return libpayment_Authorization
     */
    public function authorizeRecurringPayment(libpayment_Payment $payment)
    {
        $this->logPayment($payment);

        $client = $this->getClient();
        $service = new \Adyen\Service\Payment($client);

        $post = array(
            'amount' => $this->getAdyenAmount($payment),
            'reference' => $payment->getToken(),
            'shopperEmail' => $payment->getEmail(),
            'shopperIP' => $payment->getShopperIp(),
            'shopperReference' => $payment->getShopperReference(),
            'recurring' => $this->recurringOptions
        );

        if ($payment instanceof libpayment_CardPayment) {
            $post = $this->addPaymentCard($post, $payment);
        }

        if ($payment instanceof libpayment_BankPayment) { // SEPA direct debit
            $post = $this->addPaymentBank($post, $payment);
        }

        if ($payment instanceof libpayment_RecurringPayment) {
            $post = $this->addRecurringContract($post, $payment);
        }

        bab_debug($post);

        try {
            $result = $service->authorise($post);
        } catch (Adyen\AdyenException $e) {
            throw new libpayment_Exception(sprintf(LibPaymentAdyen_translate('Adyen error message : %s'),$e->getMessage()));
        }

        $payment->logResponse(print_r($result, true));

        if ('IdentifyShopper' === $result['resultCode']) {
            // The issuer requires the shopper's device fingerprint before the payment can be authenticated.
            // Returned for 3D Secure 2 transactions.

            $e = new LibPaymentAdyen_FingerprintRequired();
            $e->additionalData = $result['additionalData'];

            throw $e;
        }

        if ('ChallengeShopper' === $result['resultCode']) {
            // web-based flow where the device fingerprinting step is skipped
            $e = new LibPaymentAdyen_ChallengeRequired();
            $e->additionalData = $result['additionalData'];

            throw $e;
        }

        if ('RedirectShopper' === $result['resultCode']) {
            bab_debug($result);
            $e = new LibPaymentAdyen_RedirectRequired();
            $e->issuerUrl = $result['issuerUrl'];
            $e->md = $result['md'];
            $e->paRequest = $result['paRequest'];

            throw $e;
        }

        if ($result['resultCode'] === 'Authorised') {
            $authorization = $this->newAdyenAuthorization($result);

            // Do not capture the payment
            // If Adyen is set to auto capture mode, the amount will be captured from customer account
            // the setting is adyen back-office: Settings > Merchant settings

            $authorization->isCaptured = false;
            $authorization->token = $payment->getToken();

            return $authorization;
        }

        if ($result['resultCode'] === 'Refused') {
            throw new libpayment_Exception(sprintf(LibPaymentAdyen_translate('The payment card has been refused: %s'), $result['refusalReason']));
        }

        throw new libpayment_Exception(sprintf('Unexpected result code %s', $result['resultCode']));
    }



    /**
     * Store bank account information in adyen using api call
     * @url https://docs.adyen.com/developers/payout-manual#storepayoutdetailsusingtheapicall
     *
     *
     * @return array
     */
    public function storePayoutDetails(libpayment_BankDetails $bank)
    {
        $client = $this->getStorePayoutClient();
        $service = new \Adyen\Service\Payout($client);

        $post = array(
            'recurring' => array(
                'contract' => 'PAYOUT'
            ),
            'bank' => array(
                'bankName'          => $bank->bankName,
                'bic'               => $bank->bic,
                'countryCode'       => $bank->countryCode,
                'iban'              => $bank->iban,
                'ownerName'         => $bank->ownerName,
                'bankCity'          => $bank->bankCity,
                'taxId'             => $bank->taxId
            ),
            'shopperEmail'          => $bank->shopperEmail,
            'shopperReference'      => $bank->shopperReference,
            'shopperName'           => array(
                'firstName'         => $bank->shopperFirstName,
                'gender'            => $bank->shopperGender,
                'lastName'          => $bank->shopperLastName
            ),
            'dateOfBirth'           => $bank->dateOfBirth,
            'entityType'            => $bank->entityType,
            'nationality'           => $bank->nationality,
            'billingAddress' => array(
                'houseNumberOrName' => $bank->houseNumberOrName,
                'street'            => $bank->street,
                'city'              => $bank->city,
                'stateOrProvince'   => $bank->stateOrProvince,
                'country'           => $bank->country,
                'postalCode'        => $bank->postalCode
            )
        );

        try {
            $result = $service->storeDetail($post);
        } catch(\Adyen\AdyenException $e) {
            throw new libpayment_Exception(sprintf(LibPaymentAdyen_translate('Adyen error message : %s'),$e->getMessage()));
        }

        /*
        {
            "pspReference" : "9913134957760023",
            "recurringDetailReference" : "2713134957760046",
            "resultCode" : "Success"
        }
        */

        return $result;
    }



    /**
     * Get recurring contract details for the PAYOUT type
     *
     * @return libpayment_RecurringContractDetail[]
     */
    public function getPayoutDetails($shopperReference)
    {
        $client = $this->getStorePayoutClient();
        return $this->getRecurringDetails($client, 'PAYOUT', $shopperReference);
    }


    /**
     * Add recurring contract to $post
     */
    private function addRecurringContract(Array $post, libpayment_RecurringPayment $payment)
    {
        $recurringContract = $payment->getRecurringContractDetail();

        $post['recurring'] = array(
            'contract' => 'RECURRING'
        );
        $post['shopperInteraction'] = 'ContAuth';
        $post['selectedRecurringDetailReference'] = $recurringContract->id;

        return $post;
    }


    /**
     * Do a payment with the API
     *
     * @param libpayment_Payment $payment
     *
     * @return libpayment_Authorization
     */
    public function doPayment(libpayment_Payment $payment)
    {
        $this->logPayment($payment);

        $client = $this->getClient();
        $service = new \Adyen\Service\Payment($client);

        // amount in adyen format used for payment and capture
        $amount = $this->getAdyenAmount($payment);

        $params = array(

            'shopperEmail' => $payment->getEmail(),
            'shopperIP' => $payment->getShopperIp(),
            'shopperReference' => $payment->getShopperReference(),
            'amount' => $amount,
            'reference' => $payment->getToken()
        );

        if ($payment instanceof libpayment_RecurringPayment) {
            $params = $this->addRecurringContract($params, $payment);
        }

        if ($payment instanceof libpayment_CardPayment) {
            $params = $this->addPaymentCard($params, $payment);
        }

        // Test a fail

//         $config = $this->getConfig();
//         if ($config->environement === \Adyen\Environment::TEST) {
//             if (!array_key_exists('additionalData', $params)) {
//                 $params['additionalData'] = array();
//             }

//             $params['additionalData']['RequestedTestAcquirerResponseCode'] = 6;
//         }

        try {
            $result = $service->authorise($params);
        } catch(\Adyen\AdyenException $exception) {
            // This type of exception can contain a html page as message
            throw new libpayment_Exception($exception->getMessage());
        }

        $this->logResponse($payment, $result);

        // example of result:
        /*
        array(
            "pspReference" => "8814781838244635",
            "resultCode" => "Authorised",
            "authCode" => "75198"
        );

        array(
            "additionalData" => array(
                "paymentMethod" => 'visa'
            ),
            "pspReference" => "4315026059506467",
            "refusalReason" => "Not enough balance",
            "resultCode" => "Refused"
        );

        */


        $authorization = $this->newAdyenAuthorization($result);
        $authorization->isCaptured = false;
        $authorization->token = $payment->getToken();
        if (libpayment_Authorization::AUTHORIZED !== $authorization->status) {
            return $authorization;
        }

        $config = $this->getConfig();

        if ($config->autoCapture) {
            $params = array(
                'modificationAmount' => $amount,
                'originalReference' => $result['pspReference']
            );

            $service = new \Adyen\Service\Modification($client);
            try {
                $capture = $service->capture($params);
                // there is a pspReference key on capture, this is ignored
                if ('[capture-received]' === $capture['response']) {
                    $authorization->isCaptured = true;
                    // Consider the payment received if the capture is a success
                    $authorization->status = libpayment_Authorization::RECEIVED;
                } else {
                    $authorization->captureError = $capture['response'];
                }

            } catch (\Adyen\AdyenException $exception) {
                $authorization->captureError = $exception->getMessage();
            }
        }

        return $authorization;
    }



    /**
     * Confirm a third-party payout
     * the isCaptured property is used to store the confirmation status
     */
    public function confirmPayout(libpayment_Authorization $payout)
    {
        if ($payout->isCaptured) {
            // allready confirmed
            return true;
        }

        $client = $this->getReviewPayoutClient();
        $service = new \Adyen\Service\Payout($client);

        $params = array(
            'originalReference' => $payout->pspReference
        );


        try {
            $result = $service->confirmThirdParty($params);
        } catch (\Adyen\AdyenException $exception) {
            $payout->captureError = $exception->getMessage();
        }

        // example of result:
        /*
            {
                "pspReference" : "9913140798220028",
                "response" : "[payout-confirm-received]"
            }
         */

        if ('[payout-confirm-received]' === $result['response']) {
            $payout->isCaptured = true;
            return true;
        }

        return false;
    }



    /**
     * Do a payout to a third-party
     *
     * @param libpayment_Payment $payment
     *
     * @return libpayment_Authorization
     */
    public function doPayout(libpayment_RecurringPayment $payment)
    {
        $this->logPayment($payment);

        $client = $this->getStorePayoutClient();
        $service = new \Adyen\Service\Payout($client);

        // amount in adyen format used for payment and capture
        $amount = $this->getAdyenAmount($payment);

        $params = array(

            'shopperEmail' => $payment->getEmail(),
            'shopperIP' => $payment->getShopperIp(),
            'shopperReference' => $payment->getShopperReference(),
            'amount' => $amount,
            'reference' => $payment->getToken()
        );


        $recurringContract = $payment->getRecurringContractDetail();

        $params['recurring'] = array(
            'contract' => 'PAYOUT'
        );

        $params['selectedRecurringDetailReference'] = $recurringContract->id;
        //$params['selectedRecurringDetailReference'] = 'LATEST';

        try {
            $result = $service->submitThirdParty($params);
        } catch(\Adyen\AdyenException $exception) {
            throw new libpayment_Exception(__FUNCTION__.' '.$exception->getMessage());
        }

        $this->logResponse($payment, $result);

        // example of result:
        /*
        array(
        "pspReference" => "8814781838244635",
        "resultCode" => "[payout-submit-received]",
        "refusalReason" => null
        );
        */


        $authorization = $this->newAdyenAuthorization($result);
        $authorization->isCaptured = false;
        $authorization->token = $payment->getToken();


        if ('[payout-submit-received]' !== $result['resultCode']) {
            $authorization->status = libpayment_Authorization::ERROR;
            $authorization->refusalReason = $result['refusalReason'];
            return $authorization;
        }


        $authorization->status = libpayment_Authorization::AUTHORIZED;

        $config = $this->getConfig();

        if ($config->autoConfirm) {
            $this->confirmPayout($authorization);
        }

        return $authorization;
    }



    /**
     * Refund amount
     *
     * @throws libpayment_Exception if refund fail
     *
     * @param libpayment_Payment $payment   Original payment from libpayment log
     * @param string $reference             Original payment reference stored via logResponse()
     * @param libpayment_Payment $refund    The refund amount and currency (negative value)
     *
     * @return string
     */
    protected function refundAmount(libpayment_Payment $payment, $reference, libpayment_Payment $refund)
    {
        $client = $this->getClient();
        $service = new \Adyen\Service\Modification($client);

        // amount in adyen format used for payment and capture
        $amount = $this->getAdyenAmount($refund);

        $params = array(
            'modificationAmount'    => $amount,
            'reference'             => $payment->getToken(),
            'originalReference'     => $reference
        );

        $params['modificationAmount']['value'] = abs($params['modificationAmount']['value']);

        $result = $service->refund($params);
        $this->logResponse($refund, $result);

        $r = $this->newRefund();
        $r->pspReference = $result['pspReference'];
        $r->token = $refund->getToken();


        if ('[refund-received]' !== $result['response']) {
            $r->status = libpayment_Refund::ERROR;
            $r->refusalReason = $result['response'];

            throw new libpayment_Exception($result['response']);
        }

        $r->status = libpayment_Refund::RECEIVED;

        return $r;
    }





    /**
     *
     * {@inheritDoc}
     * @see Func_Payment::getRecurringContractDetails()
     *
     * @return libpayment_RecurringContractDetail[]
     */
    protected function getRecurringDetails($client, $contractType, $shopperReference)
    {

        $service = new \Adyen\Service\Recurring($client);

        $result = $service->listRecurringDetails(array(
            'shopperReference' => $shopperReference,
            'recurring' => array(
                'contract' => $contractType
            )
        ));

        $contracts = array();

        if (!isset($result['details'])) {
            return $contracts;
        }

        foreach ($result['details'] as $d) {
            $detail = $d['RecurringDetail'];

            $contract = $this->newRecurringContractDetail();

            $contract->id = $detail['recurringDetailReference'];
            $contract->paymentMean = $detail['variant'];

            if (isset($detail['card'])) {
                $contract->card = $contract->newCard();
                $contract->card->number = $detail['card']['number']; // this is not the full number
                $contract->card->expiryMonth = $detail['card']['expiryMonth'];
                $contract->card->expiryYear = $detail['card']['expiryYear'];
                if (isset($detail['card']['cvc'])) {
                    $contract->card->cvc = $detail['card']['cvc']; // probably not set for security reason
                }
                $contract->card->holderName = $detail['card']['holderName'];
            }


            if (isset($detail['bank'])) {

                $bank = $detail['bank'];

                $contract->bank = $contract->newBankDetails();

                if (isset($bank['bankAccountNumber'])) {
                    $contract->bank->bankAccountNumber  = $bank['bankAccountNumber'];
                }

                if (isset($bank['bankLocationId'])) {
                    $contract->bank->bankLocationId     = $bank['bankLocationId'];
                }

                if (isset($bank['bankName'])) {
                    $contract->bank->bankName           = $bank['bankName'];
                }

                if (isset($bank['bic'])) {
                    $contract->bank->bic                = $bank['bic'];
                }

                if (isset($bank['countryCode'])) {
                    $contract->bank->countryCode        = $bank['countryCode'];
                }

                if (isset($bank['iban'])) {
                    $contract->bank->iban               = $bank['iban'];
                }

                if (isset($bank['ownerName'])) {
                    $contract->bank->ownerName          = $bank['ownerName'];
                }
            }

            $creationDate = str_replace('T', ' ', $detail['creationDate']);
            $creationDate = preg_replace('/\+\d\d:\d\d/', '', $creationDate);

            $contract->creationDate = BAB_DateTime::fromIsoDateTime($creationDate);

            if (isset($detail['firstPspReference'])) {
                // this is not working for payout storeDetail
                // this work if the is an authorization attached to recurring contract creation
                $contract->firstPspReference = $detail['firstPspReference'];
            }

            $contract->shopperReference = $shopperReference;



            $contracts[] = $contract;
        }

        return $contracts;
    }



    /**
     *
     * {@inheritDoc}
     * @see Func_Payment::getRecurringContractDetails()
     *
     * @return libpayment_RecurringContractDetail[]
     */
    public function getRecurringContractDetails($shopperReference)
    {
        $client = $this->getClient();
        return $this->getRecurringDetails($client, 'RECURRING', $shopperReference);
    }




    protected function disableRecurringContract(\Adyen\Client $client, libpayment_RecurringContractDetail $contractDetail)
    {

        $service = new \Adyen\Service\Recurring($client);

        $result = $service->disable(array(
            'shopperReference' => $contractDetail->shopperReference,
            'recurringDetailReference' => $contractDetail->id
        ));

        return ('[detail-successfully-disabled]' === $result['response']);
    }



    /**
     * Disable a recurring contract detail
     * @param libpayment_RecurringContractDetail $contractDetail
     *
     * @return bool
     */
    public function disableRecurringContractDetail(libpayment_RecurringContractDetail $contractDetail)
    {
        $client = $this->getClient();
        return $this->disableRecurringContract($client, $contractDetail);
    }




    /**
     * Disable a payout recurring contract detail
     * @param libpayment_RecurringContractDetail $contractDetail
     *
     * @return bool
     */
    public function disablePayoutDetail(libpayment_RecurringContractDetail $contractDetail)
    {
        $client = $this->getStorePayoutClient();
        return $this->disableRecurringContract($client, $contractDetail);
    }



    /**
     * Get the configured CSE url
     * @return string
     */
    public function getCseUrl()
    {
        $config = $this->getConfig();
        return $config->cseUrl;
    }

    /**
     * Download adyen report url to local filename using credentials in settings
     * @param string $url
     * @param string $filename
     * @return bool
     */
    public function downloadReport($url, $filename)
    {
        $config = $this->getConfig();
        $fp = fopen ($filename, 'w+');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 600);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERPWD, $config->reportUsername.':'.$config->reportPassword);
        $r = curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        return $r;
    }

    public function isValidNotificationHMAC($params)
    {
        $config = $this->getConfig();
        $hmac = new \Adyen\Util\HmacSignature();
        return $hmac->isValidNotificationHMAC($config->notificationHmacKey, $params);
    }
}