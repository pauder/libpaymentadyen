<?php 


class LibPaymentAdyen_FingerprintRequired extends Exception
{
    public $additionalData = null;
    
    /**
     * Get value for the threeDSMethodData input field
     * @return string
     */
    public function getThreeDSMethodDataValue()
    {
        return base64_encode(json_encode(array(
            'threeDSServerTransID' => $this->additionalData['threeds2.threeDSServerTransID'],
            'threeDSMethodNotificationURL' => 'https://www.hoper.fr/3ds.php'
        )));
    }
    
    public function getPostAction()
    {
        return $this->additionalData['threeds2.threeDSMethodURL'];
    }
    
    public function getThreeDS2Token()
    {
        return $this->additionalData['threeds2.threeDS2Token'];
    }
}

class LibPaymentAdyen_ChallengeRequired extends Exception
{
    public $additionalData = null;
    
    /**
     * Get value for the creq input field
     * @return string
     */
    public function getCreqValue()
    {
        return base64_encode(json_encode(array(
            'threeDSServerTransID' => $this->additionalData['threeds2.threeDS2ResponseData.threeDSServerTransID'],
            'acsTransID' => $this->additionalData['threeds2.threeDS2ResponseData.acsTransID'],
            'messageVersion' => $this->additionalData['threeds2.threeDS2ResponseData.messageVersion'],
            'challengeWindowSize' => '04',
            'messageType' => 'CReq'
        )));
    }
    
    public function getPostAction()
    {
        return $this->additionalData['threeds2.threeDS2ResponseData.acsURL'];
    }
    
    public function getThreeDS2Token()
    {
        return $this->additionalData['threeds2.threeDS2Token'];
    }
}

class LibPaymentAdyen_RedirectRequired extends Exception
{
    public $issuerUrl = null;
    public $md = null;
    public $paRequest = null;
}

class LibPaymentAdyen_AuthenticationFinished extends Exception
{
    public $additionalData = null;
    
    /**
     * The value for the 3D Secure 2 authentication session. The returned value is a Base64-encoded 20-byte array.
     * @return string
     */
    public function getAuthenticationValue()
    {
        return $this->additionalData['threeds2.threeDS2Result.authenticationValue'];
    }
    
    /**
     * The unique transaction identifier assigned by the Directory Server to identify a single transaction.
     * @return string
     */
    public function getDsTransID()
    {
        return $this->additionalData['threeds2.threeDS2Result.dsTransID'];
    }
    
    /**
     * The Electronic Commerce Indicator returned from the schemes for the 3D Secure 2 payment session.
     * @return string
     */
    public function getEci()
    {
        return $this->additionalData['threeds2.threeDS2Result.eci'];
    }
}
