<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'ac571a75fa6b7a754fd994057f54047852aab99e',
    'name' => 'ovidentia/libpaymentadyen-dep',
  ),
  'versions' => 
  array (
    'adyen/php-api-library' => 
    array (
      'pretty_version' => '8.0.0',
      'version' => '8.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e5332997e0b0012fbbe775b913134b15aed0cfb',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bad29cb8d18ab0315e6c477751418a82c850d558',
    ),
    'ovidentia/libpaymentadyen-dep' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'ac571a75fa6b7a754fd994057f54047852aab99e',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
      ),
    ),
  ),
);
