��    %      D  5   l      @     A     S     i     �  ;   �  -   �     �  #        7  z   >  6   �     �     �                 !   ,  (   N     w     |     �     �     �     �  
   �     �     �  "   �       %     =   @  	   ~     �     �  ,   �     �  '  �     !	     ;	     X	     t	  T   �	  +   �	  &   

  3   1
     e
  �   m
  ?   	  	   I     S     _      h     �  %   �  2   �     �     �                $     B  
   O     Z     w  %   �     �  *   �  C   �     /     A  &   S  '   z  +   �                           "                        #                              
                                            	                    !                $   %               Add configuration Adyen Payment Gateway Adyen error message : %s Application name Application name given to adyen api, default to babSiteName Are you sure you want to delete the TPE '%s'? Auto capture payments Auto confirm payouts to third-party Cancel Check this setting if auto-capture is disabled on adyen back-office, recurring contract autorizations will not be captured Client Side Encryption javascript file hosted by Adyen Delete Description Edit Edit TPE configuration Environement HMAC key for Hosted Payment Pages HMAC key for notifications signed fields List List of configured TPE Merchant account Name New TPE configuration Password Production Save configuration Set default Skin code for Hosted Payment Pages Test The payment card has been refused: %s The selected configuration "%s" does not contain informations User name Username You must specify a password. You must specify a unique name for this TPE. You must specify a user name. Project-Id-Version: LibPaymentAdyen
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-09-01 07:48+0200
Last-Translator: Paul de Rosanbo <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: LibPaymentAdyen_translate;LibPaymentAdyen_translate:1,2
X-Generator: Poedit 2.3
X-Poedit-SearchPath-0: programs
 Ajouter une configuration Passerelle de paiement Adyen Message d'erreur Adyen : %s Nom de l'application Nom de l'application fourni à l'API Adyen, le nom du site sera utilisé par défaut Voulez-vous vraiment supprimer le TPE "%s"? Capturer automatiquement les paiements Confirmer automatiquement les payouts vers un tiers Annuler Cocher cette case si la capture automatique est désactivé dans le back-office Adyen, les autorisations de paiements récurrents ne serons pas capturées. Fichier javascript "Client Side Encryption" hébergé par Adyen Supprimer Description Modifier Modifier la configuration du TPE Environnement Clé HMAC pour "Hosted Payment Pages" Clé HMAC pour les champs de notifications signés Liste Liste des TPE configurés Compte marchant Nom Nouvelle configuration de TPE Mot de passe Production Enregistrer la configuration Choisir comme TPE par défaut Code skin pour "Hosted Payment Pages" Test La carte de paiement à été refusée: %s La configuration sélectionnée "%s" ne contient pas d'informations Nom d'utilisateur Nom d'utilisateur Vous devez spécifier un mot de passe. Vous devez indiquer un nom pour ce TPE. Vous devez spécifier un nom d'utilisateur. 